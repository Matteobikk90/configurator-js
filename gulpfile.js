const gulp = require("gulp");
const autoprefixer = require("gulp-autoprefixer");
const browserSync = require("browser-sync").create();
const reload = browserSync.reload;
const sass = require("gulp-sass");
const rename = require("gulp-rename");
const cleanCSS = require("gulp-clean-css");
const sourcemaps = require("gulp-sourcemaps");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");
const lineec = require("gulp-line-ending-corrector");

const stylesWatchFiles = "./src/assets/scss/*.scss";
const jsWatchFiles = "./src/assets/js/*.js";
const htmlWatchFile = ["./index.html", "./src/assets/html/*.html"];

const styleMinCSS = "./src/assets/css";
const minJS = "./src/assets/js/min";

function style() {
  return gulp
    .src(stylesWatchFiles)
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(autoprefixer("last 2 versions"))
    .pipe(lineec())
    .pipe(cleanCSS())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(styleMinCSS));
}

function javascript() {
  return gulp
    .src(jsWatchFiles)
    .pipe(
      babel({
        presets: ["@babel/env"]
      })
    )
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(lineec())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(minJS));
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
  gulp.watch(stylesWatchFiles, gulp.series([style])).on("change", reload);
  gulp.watch(jsWatchFiles, gulp.series([javascript])).on("change", reload);
  gulp.watch(htmlWatchFile).on("change", reload);
}

exports.style = style;
exports.javascript = javascript;
exports.watch = watch;

const build = gulp.parallel(watch);
gulp.task("default", build);
