const nextBtn = document.querySelectorAll(".nextBtn");
const prevBtn = document.querySelectorAll(".prevBtn");
const resetBtn = document.querySelector("#resetBtn");

const next = e => {
  const { classList, nextElementSibling } = e.target.parentElement;
  classList.add("fadeOut");
  classList.remove("fadeIn");
  nextElementSibling.classList.add("fadeIn");
};

const prev = e => {
  const { classList, previousElementSibling } = e.target.parentElement;
  classList.remove("fadeIn");
  previousElementSibling.classList.add("fadeIn");
};

const reset = () => {
  window.location.reload();
};

nextBtn.forEach(btn => btn.addEventListener("click", next));
prevBtn.forEach(btn => btn.addEventListener("click", prev));
resetBtn.addEventListener("click", reset);
